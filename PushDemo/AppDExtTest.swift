//
//  AppDExtTest.swift
//  PushDemo
//
//  Created by Tilo on 2021-09-25.
//

import Foundation
import Alamofire

extension AppDelegate {
    
    func getSound() -> String {
        return "fart-2.wav"
    }
    func soundTestWithAlamoFire() {
        let toDeviceID = AppDelegate.DEVICEID
        let fromSender: String?
        fromSender = "Tilo"
        let sentSound = "sendasound test"
        
        
        print("toDeviceID: ", toDeviceID)
        let title = ""
        let body = fromSender! + " sent you '" + sentSound + "'"
        let sound = getSound()
        var headers:HTTPHeaders = HTTPHeaders()
        headers = ["Content-Type":"application/json","Authorization":"key=\(AppDelegate.SERVERKEY)"]

        // badge is 0 because I would otherwise have to store the counter on firebase to increament it
        let notification = ["to": toDeviceID, "notification":["body":body,"title":title,"badge":0,"sound":sound]] as [String:Any]

        AF.request(AppDelegate.NOTIFICATION_URL as URLConvertible, method: .post as HTTPMethod, parameters: notification, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            print(response)
        }
    }
    
}


func soundTest() {
    
    let URLString = AppDelegate.NOTIFICATION_URL
    
    let configuration = URLSessionConfiguration.default
    let session = URLSession(configuration: configuration)
    let url = URL(string: URLString)
    //let url = NSURL(string: urlString as String)
    var request : URLRequest = URLRequest(url: url!)
    
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("Authorization", forHTTPHeaderField: "key=\(AppDelegate.SERVERKEY)")
    
    
    let dataTask = session.dataTask(with: url!) {
       data,response,error in
       // 1: Check HTTP Response for successful GET request
       guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
       else {
          print("error: not a valid http response")
          return
       }
       switch (httpResponse.statusCode) {
          case 200:
             //success response.
             break
          case 400:
             break
          default:
             break
       }
    }
    dataTask.resume()
}
